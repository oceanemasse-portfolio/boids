# Unity 3D  Scripting CSharp

Avril 2022

Ce jeu est un prototype mettant en avant un algorithme basé sur les "boids" (algorithme de vie artificielle) dans le langage C#.
Le joueur incarne ici l'Orc, et essaye d'approche le trésor dans le nid du Dragon, protégé par une meute de loup. 
Cette meute de loups est gérée par l'algorithme des "boids", en respectant une directive majeure :
Ils poursuivent le joueur et assure la protection du trésor et on retrouve donc des mécaniques d'attraction, répulsion et alignement.

L'environnement a été créé à la main à l'aide d'Unity, auquel j'ai pu ajouter des textures et peindre sur le terrain.
Divers assets importés viennent compléter l'environnement.
Actuellement, ni l'environnement ni les GameObjets ne disposent de collider / rigidbody, de ce fait vous allez traverser
le décor, les pnj se superposent ; ce qui sera bien évidemment à améliorer.
Par ailleurs, j'ai ajouté des animations sur les loups, dragon et joueurs ; en faisant moi-même leurs animation
controller et modifiant le script en conséquence.
_ les loups courent ou sont sur place
_ le dragon rugit ou attaque si le joueur est proche du trésor
_ l'orc court ou reste sur place

##############

Déplacez vous à l'aide de ZQSD et tournez la caméra grâce à A et E.
Il n'y a pas de but actuel, déplacez vous et observez votre environnement !

##############

Dans le code source :

MoveTransformPositionWithInputSmoothlyAndRotation est le Character controller

WolfPack contient les fonctions et le main permettant aux loups de se déplacer, suivre le joueur, ...

WolfManager se charge d'instancier les loups et les rends consultables

dragon est utilisé uniquement pour l'animation du dragon

